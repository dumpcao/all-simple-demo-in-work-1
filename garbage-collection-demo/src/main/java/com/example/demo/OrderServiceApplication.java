package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@RestController
@Slf4j
public class OrderServiceApplication {
	static Object object;

	static List<Object> list = new ArrayList<>();
	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}

	@RequestMapping("/")
	public  void test() throws InterruptedException {
		byte[] bytes = new byte[1024 * 1024];

		Thread.sleep(1000);
	}

	@RequestMapping("/link")
	public  void staticlink(@RequestParam("value") Integer value) throws InterruptedException {
		byte[] bytes = new byte[value * 1024 * 1024];
		object = bytes;
	}

	@RequestMapping("/addlist")
	public  void lust(@RequestParam("value") Integer value) throws InterruptedException {
		byte[] bytes = new byte[value * 1024 * 1024];
		list.add(bytes);
	}

	@RequestMapping("/clearlist")
	public  void clearlist() throws InterruptedException {
		list.clear();
	}


}
