package com.example.demo.metaspaceoom;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibNoCache {
    public static void main(String[] args) {
        long counter = 0;

        while (true) {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(Car.class);
            enhancer.setUseCache(false);
            enhancer.setCallback(new MethodInterceptor() {
                @Override
                public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                    if (method.getName().equals("run")) {
                        System.out.println("check security first");
                        return proxy.invokeSuper(obj, args);
                    }
                    return null;
                }
            });

            Car o = (Car) enhancer.create();
            o.run();

            System.out.println("create " + counter++ + " sub class");
        }
    }

    static class  Car{
        public void run() {
            System.out.println("汽车启动了");
        }
    }

}
