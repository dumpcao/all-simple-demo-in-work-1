package com.example.demo;

import com.example.date.util.CommonDateUtil;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class GisUtilImpl implements IGisUtilInterface{

    @Override
    public  String getFormattedDate(String date) {
        String v1 = CommonDateUtil.format(date);
        log.info("invoke common v1,get result:{}", v1);

        return v1;
    }

}
