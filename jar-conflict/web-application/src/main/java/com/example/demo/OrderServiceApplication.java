package com.example.demo;

import com.example.date.util.CommonDateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.loader.archive.Archive;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
@Slf4j
public class OrderServiceApplication {
	/**
	 * 中间件使用的classloader
	 */
	static ClassLoader delegatingClassloader;

	public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Method method = loader.getClass().getMethod("getMiddleWareClassPathArchives");
		List<URL> middleWareUrls = (List<URL>) method.invoke(loader);

		delegatingClassloader = new CustomMiddleWareClassloader(middleWareUrls.toArray(new URL[0]),
				loader);

		for (URL middleWareUrl : middleWareUrls) {
			log.info("middle ware classloader url:{}",middleWareUrl);
		}



		SpringApplication.run(OrderServiceApplication.class, args);
	}

	@RequestMapping("/")
	public  void test() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		Class<?> middleWareImplClass = delegatingClassloader.loadClass("com.example.demo.GisUtilImpl");
		IGisUtilInterface iGisUtilInterface = (IGisUtilInterface) middleWareImplClass.newInstance();
		String middleWareResult = iGisUtilInterface.getFormattedDate("version:");

		log.info("middle ware result:{}",middleWareResult);

		String result = CommonDateUtil.format("version:");
		log.info("application result:{}",result);

		ClassLoader loader = OrderServiceApplication.class.getClassLoader();
		String name = loader.getClass().getName();

	}

}
