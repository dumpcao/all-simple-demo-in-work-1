package com.example.demo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.net.URLClassLoader;

@Data
@Slf4j
public class CustomMiddleWareClassloader extends URLClassLoader {
    ClassLoader classLoader;

    public CustomMiddleWareClassloader(URL[] urls, ClassLoader parent) {
        super(urls);
        classLoader = parent;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        /**
         * 先自己来加载
         */
        try {
            Class<?> clazz = findClass(name);
            if (clazz != null) {
                return clazz;
            }
            throw new ClassNotFoundException(name);
        } catch (Exception e) {
            /**
             * 在中间件自己的jar包里找不到，就交给自己的parent
             */
            return classLoader.loadClass(name);
        }
    }


}
