/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.demo;

import org.springframework.boot.loader.LaunchedURLClassLoader;
import org.springframework.boot.loader.Launcher;
import org.springframework.boot.loader.jar.Handler;

import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarFile;


public class CustomLaunchedURLClassLoader extends LaunchedURLClassLoader {

	List<URL> middleWareClassPathArchives;

	/**
	 * Create a new {@link LaunchedURLClassLoader} instance.
	 *
	 * @param urls   the URLs from which to load classes and resources
	 * @param parent the parent class loader for delegation
	 */
	public CustomLaunchedURLClassLoader(URL[] urls, ClassLoader parent) {
		super(urls, parent);
	}


	public List<URL> getMiddleWareClassPathArchives() {
		return middleWareClassPathArchives;
	}

	public void setMiddleWareClassPathArchives(List<URL> middleWareClassPathArchives) {
		this.middleWareClassPathArchives = middleWareClassPathArchives;
	}
}
