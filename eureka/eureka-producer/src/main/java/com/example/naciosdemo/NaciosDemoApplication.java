package com.example.naciosdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NaciosDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NaciosDemoApplication.class, args);
	}

}
