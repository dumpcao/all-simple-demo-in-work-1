package com.example.reqvo;

import lombok.Data;

@Data
public class LoginReqVO {
    private String userName;

    private String password;
}
