package com.example.respvo;

import lombok.Data;

@Data
public class LoginRespVO {
    private String userName;

    private Integer age;

    private String token;
}
