/*
SQLyog v10.2 
MySQL - 5.7.27-log : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test`;

/*Table structure for table `cached_http_req_to_resend` */

DROP TABLE IF EXISTS `cached_http_req_to_resend`;

CREATE TABLE `cached_http_req_to_resend` (
  `http_req_id` bigint(20) NOT NULL COMMENT '主键',
  `req_type` tinyint(4) NOT NULL COMMENT '请求类型，1：推送待处置结果给第三方系统',
  `third_sys_feign_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '第三方系统的名称，和feignClient的保持一致',
  `http_req_body` varchar(4000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '要访问的uri，此处为相对路径，对应要访问的系统的uri',
  `current_state` tinyint(4) DEFAULT NULL COMMENT '该请求当前状态,1：成功；2：失败；3：待处理;4:失败次数过多，放弃尝试',
  `fail_count` tinyint(4) DEFAULT NULL COMMENT '截止目前，失败次数；超过指定次数后，将跳过该请求',
  `success_time` datetime DEFAULT NULL COMMENT '请求成功发送的时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `related_entity_id` bigint(21) DEFAULT NULL COMMENT '相关的实体的id，比如在推送待处置警情时，这个id为处警id',
  PRIMARY KEY (`http_req_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Table structure for table `common_distributed_lock` */

DROP TABLE IF EXISTS `common_distributed_lock`;

CREATE TABLE `common_distributed_lock` (
  `lock_id` bigint(20) NOT NULL,
  `lock_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` int(11) NOT NULL COMMENT '0:未锁定；1：锁定',
  `expire_time` datetime DEFAULT NULL COMMENT '如果被锁定，则锁的过期时间为该时间；获取锁时，必须设置这个值',
  `owner` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
