package com.example.aop;

import com.example.message.CommonMessageType;
import com.example.message.Message;
import com.example.message.MessageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * 
 * 统一异常处理
 *
 */
@Controller
// 监听所有使用@Controller注解的类抛出的异常
@ControllerAdvice(annotations = { Controller.class, RestController.class })
public class GlobalExceptionHandler {

	private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 异常中的code和errmsg将分别作为返回的msg对象的code、message属性
     * @param e
     * @return
     */
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Message handleBusinessException(RuntimeException e) {
		logger.error("BusinessException throwed", e);
		return MessageFactory.createMessage(CommonMessageType.UNKNOWN);
	}


}
