package com.example.scheduledtask;

import com.example.service.ICachedHttpReqToResendService;
import com.example.service.ICommonDistributedLockService;
import com.example.util.DISTRIBUTED_LOCK_ENUM;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 检测是否已经恢复为非本地模式，如果是的话，就把缓存的请求给发过去
 */
@Component
@Slf4j
public class SendCachedFeignReqTask {


    @Autowired
    private Environment environment;

    @Autowired
    private ICommonDistributedLockService iCommonDistributedLockService;


    @Autowired
    private ICachedHttpReqToResendService iCachedHttpReqToResendService;


    @Scheduled(cron = "0/30 * * * * ?  ")
    public void sendCachedFeignReq() {
        Thread.currentThread().setName("SendCachedFeignReqTask");
        log.info("start sendCachedFeignReq");

        /**
         * 1、获取锁
         */
        boolean success = iCommonDistributedLockService.tryLock(DISTRIBUTED_LOCK_ENUM.SEND_CACHED_FEIGN_REQ_TO_REMOTE_SERVER.lockName,
                DISTRIBUTED_LOCK_ENUM.SEND_CACHED_FEIGN_REQ_TO_REMOTE_SERVER.expireDurationInSeconds);
        if (!success) {
            log.info("get lock failed,skip processing");
            return;
        }

        /**
         * 进行业务逻辑处理
         */
        iCachedHttpReqToResendService.processCachedFeignReqForLoginLogout();

        /**
         * 释放锁
         */
        iCommonDistributedLockService.unlock(DISTRIBUTED_LOCK_ENUM.SEND_CACHED_FEIGN_REQ_TO_REMOTE_SERVER.lockName);

    }



}
