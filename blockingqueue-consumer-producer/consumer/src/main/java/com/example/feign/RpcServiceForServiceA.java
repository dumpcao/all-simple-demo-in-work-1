package com.example.feign;

import com.example.FeignServiceA;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "SERVICE-A")
public interface RpcServiceForServiceA extends FeignServiceA {
}
