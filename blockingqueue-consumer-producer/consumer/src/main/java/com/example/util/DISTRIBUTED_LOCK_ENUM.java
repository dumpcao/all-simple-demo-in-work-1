package com.example.util;

public  enum DISTRIBUTED_LOCK_ENUM {
    SEND_CACHED_FEIGN_REQ_TO_REMOTE_SERVER("SEND_CACHED_FEIGN_REQ_TO_REMOTE_SERVER",30,"当failover.mode为on时，" +
            "就会在本地登录、登出等；等后续恢复了，再往第三方系统推送；定时任务会去执行这个推送的过程，这个锁就是此时会用到"),
    ;
    public final String lockName;

    public final Integer expireDurationInSeconds;

    public final String desc;

    DISTRIBUTED_LOCK_ENUM(String lockName, Integer expireDurationInSeconds, String desc) {
        this.lockName = lockName;
        this.expireDurationInSeconds = expireDurationInSeconds;
        this.desc = desc;
    }
}
