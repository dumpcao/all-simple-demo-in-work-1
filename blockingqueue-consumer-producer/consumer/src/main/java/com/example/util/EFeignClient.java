package com.example.util;

public enum EFeignClient {
    SERVICE_A("SERVICE-A"),
    ;

    private String name;

    EFeignClient(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
