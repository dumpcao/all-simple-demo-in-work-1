package com.example.util;

import com.example.message.CommonMessageType;
import com.example.message.Message;
import org.springframework.lang.Nullable;

import java.util.Objects;


/**
 * desc:
 *
 * @author :
 * creat_date: 2019/11/13 0013
 * creat_time: 10:27
 **/
public class FeignMsgUtils {
    /**
     * 获取消息体
     * @param message
     * @param <T>
     * @return
     */
    @Nullable
    public  static <T> T extractDataBody(Message<T> message) {
        if (message == null) {
            return null;
        }
        boolean b = Objects.equals(CommonMessageType.SUCCESS.getCode(), message.getCode());
        if (!b) {
            return null;
        }

        return message.getData();
    }

    public  static boolean isSuccess(Message message) {
        if (message == null) {
            return false;
        }
        boolean b = Objects.equals(CommonMessageType.SUCCESS.getCode(), message.getCode());
        if (!b) {
            return false;
        }

        return true;
    }


}
