package com.example.controller;

import com.example.message.Message;
import com.example.respvo.LoginRespVO;
import com.example.service.IDemoService;
import com.example.util.BaseController;
import com.example.reqvo.LoginReqVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class DemoController extends BaseController {
    @Autowired
    private IDemoService iDemoService;

    /**
     * requestBody设置为required = false
     * 方便测试
     * @param reqVO
     * @return
     */
    @RequestMapping("/login.do")
    public Message<Object> login(@RequestBody(required = false) LoginReqVO reqVO){
        if (reqVO == null) {
            reqVO = new LoginReqVO();
            reqVO.setUserName("zhangsan");
            reqVO.setPassword("123456");
        }

        LoginRespVO login = iDemoService.login(reqVO);
        return successResponse(login);
    }
}
