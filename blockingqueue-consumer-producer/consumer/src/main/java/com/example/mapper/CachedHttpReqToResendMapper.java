package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.model.CachedHttpReqToResend;

/**
 * <p>
 * cached_http_req_to_resend Mapper 接口
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-10-31
 */
public interface CachedHttpReqToResendMapper extends BaseMapper<CachedHttpReqToResend> {

}
