package com.example.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.example.model.CommonDistributedLock;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2020-07-24
 */
public interface CommonDistributedLockMapper extends BaseMapper<CommonDistributedLock> {
    @Update("UPDATE common_distributed_lock SET state = 1,expire_time = #{expireTime},owner=#{owner} ${ew.customSqlSegment}")
    Integer tryLock(@Param("expireTime") Date expireTime, @Param("owner") String owner, @Param(Constants.WRAPPER) QueryWrapper wrapper);


    @Update("UPDATE common_distributed_lock SET state = 0,expire_time = null, owner = null where lock_name = #{lockName} and owner = #{owner}")
    Integer unlock(@Param("lockName") String lockName, @Param("owner") String owner);
}
