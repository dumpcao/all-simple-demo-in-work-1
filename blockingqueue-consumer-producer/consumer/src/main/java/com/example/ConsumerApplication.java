package com.example;

import com.example.service.ICommonDistributedLockService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.example.mapper")
@ComponentScan("com.example")
@EnableFeignClients
@EnableScheduling
public class ConsumerApplication implements CommandLineRunner {
	@Autowired
	private ICommonDistributedLockService iCommonDistributedLockService;

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		iCommonDistributedLockService.initLock();
	}
}
