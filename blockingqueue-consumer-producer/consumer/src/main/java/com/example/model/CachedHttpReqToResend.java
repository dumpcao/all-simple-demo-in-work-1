package com.example.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2020-03-11
 * 该文件由ftl模板生成，如需批量修改，可直接修改模板后重新生成，
 * 参考module：mybatis_plus_generator
 */
@Data
@Accessors(chain = true)
public class CachedHttpReqToResend {


    /**
     * 向serviceA发login请求
     * 每个常量，对应一个请求
     */
    public static final int REQ_TYPE_LOGIN_TO_SERVICE_A = 100;


    public static final int CURRENT_STATE_SUCCESS = 1;
    public static final int CURRENT_STATE_FAIL = 2;
    public static final int CURRENT_STATE_INIT = 3;
    public static final int CURRENT_STATE_FAIL_TOO_MANY_TIMES = 4;

    /**
     * 主键
     */
    @TableId(value = "http_req_id", type = IdType.ID_WORKER)
    private Long httpReqId;


    /**
     * 请求类型，取值参考：
     * {@link CachedHttpReqToResend#REQ_TYPE_LOGIN_TO_SERVICE_A}等常量
     */
    private Integer reqType;


    /**
     * 第三方系统的名称，和feignClient的保持一致
     */
    private String thirdSysFeignName;


    /**
     * 要访问的uri，此处为相对路径，对应要访问的系统的uri
     */
    private String httpReqBody;


    /**
     * 该请求当前状态,1：成功；2：失败；3：待处理;4:失败次数过多，放弃尝试
     */
    private Integer currentState;


    /**
     * 截止目前，失败次数；超过指定次数后，将跳过该请求
     */
    private Integer failCount;


    /**
     * 请求成功发送的时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date successTime;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /**
     * 相关的实体的id，比如在推送待处置警情时，这个id为处警id
     */
    private Long relatedEntityId;



}
