package com.example.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2020-07-24
 * 该文件由ftl模板生成，如需批量修改，可直接修改模板后重新生成，
 * 参考module：mybatis_plus_generator
 */
@Data
@Accessors(chain = true)
public class CommonDistributedLock {
    public static final int LOCKED = 1;
    public static final int UNLOCKED = 0;


    @TableId(value = "lock_id", type = IdType.ID_WORKER)
    private Long lockId;


    private String lockName;


    /**
    * 0:未锁定；1：锁定
    */
    private Integer state;


    /**
     * 如果被锁定，则锁的过期时间为该时间；获取锁时，必须设置这个值
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expireTime;


    private String owner;



}
