package com.example.service;


import com.example.model.CachedHttpReqToResend;

import java.util.Collection;

public interface CachedHttpReqProcessor {
    /**
     * 该处理器是否支持处理该service
     * @param feignClientName
     * @return
     */
    boolean support(String feignClientName);


    /**
     * 具体的处理逻辑
     * @param list
     */
    void process(Collection<CachedHttpReqToResend> list);

    /**
     * worker线程的名字
     * @return
     */
    String getThreadName();
}
