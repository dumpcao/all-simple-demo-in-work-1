package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.model.CommonDistributedLock;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2020-07-24
 */
public interface ICommonDistributedLockService extends IService<CommonDistributedLock> {

    boolean tryLock(String rePullScmDataWhenConsumeFailed, int expireDurationInSeconds);

    void initLock();

    void unlock(String lockName);
}
