package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.model.CachedHttpReqToResend;
import com.example.reqvo.LoginReqVO;

/**
 * <p>
 * cached_http_req_to_resend 服务类
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-10-31
 */
public interface ICachedHttpReqToResendService extends IService<CachedHttpReqToResend> {


    void saveLoginReqWhenFailOver(LoginReqVO reqVO);

    void processCachedFeignReqForLoginLogout();
}
