package com.example.service;

import com.example.reqvo.LoginReqVO;
import com.example.respvo.LoginRespVO;

public interface IDemoService {
    LoginRespVO login(LoginReqVO reqVO);
}
