/**
 *
 */
package com.example.message;


public enum CommonMessageType implements IMessageType {
    /**
     * 100000, 请求成功
     */
    SUCCESS("100000", "请求成功"),

    /**
     * 109999, 未知异常
     */
    UNKNOWN("109999", "请求失败，未知异常");


    /**
     * 消息编码
     */
    private String code;

    /**
     * 消息内容
     */
    private String content;

    CommonMessageType(String code, String content) {
        this.code = code;
        this.content = content;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getContent() {
        return content;
    }
}
