/**
 *
 */
package com.example.message;

import lombok.Data;

/**
 * 为了兼容老代码：
 * 该类对象可以通过new的方式生成，但更建议由
 * {@link MessageFactory}生成
 *
 * @author 李涛
 * 修改by 曹坤亮
 */
@Data
public class Message<T> {
    private String code;

    private String message;

    private T data;


    public Message() {

    }

    public Message(String code, T data) {
        this.code = code;
        this.data = data;
    }


    public Message(IMessageType type) {
        this.code = type.getCode();
        //新增消息说明
        this.message = type.getContent();
    }

    public Message(String code) {
        this.code = code;
    }

    public Message(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }




}
