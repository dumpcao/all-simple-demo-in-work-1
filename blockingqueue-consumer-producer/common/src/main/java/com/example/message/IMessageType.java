/******************************************************************
 * IIMessageType.java
 * Copyright 2017 by CEIEC Company. All Rights Reserved.
 * CreateDate：2017年9月4日
 * Author：NieLixiang
 * Version：1.0.0
 ******************************************************************/

package com.example.message;

/**
 * 主要用于提供消息枚举类的接口及公用消息码
 *
 * @author caokunliang
 */
public interface IMessageType {

    /**
     * 获取消息码
     * @return
     */
	String getCode();

    /**
     * 获取消息码
     * @return
     */
	String getContent();
}
