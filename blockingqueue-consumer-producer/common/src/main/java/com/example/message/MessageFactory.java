/**
 * 
 */
package com.example.message;

/**
 * 消息工厂类
 * 
 *
 *
 */
public class MessageFactory {

	private MessageFactory() {

	}

	/**
	 * 创建指定类型的消息,消息数据为空
	 * 
	 * @param type 消息类型
	 * @return
	 */
	public static Message createMessage(IMessageType type) {
		return createMessage(type,null);
	}

    /**
     * 创建指定类型的消息,携带消息内容
     *
     * @param type 消息类型
     * @param  data 消息内容
     * @return
     */
    public static Message createMessage(IMessageType type,Object data) {
        return new Message(type.getCode(),type.getContent(),data);
    }

    /**
     * 创建指定类型的消息,携带消息内容
     *
     * @return
     */
    public static Message createMessage(String code,String content,Object data) {
        return new Message(code,content,data);
    }
	
	/**
	 * 创建成功消息
	 * 
	 * @return
	 */
	public static Message createSuccessMsg() {
		return createMessage(CommonMessageType.SUCCESS);
	}

    /**
     * 创建成功消息,携带内容
     *
     * @return
     */
    public static Message createSuccessMsg(Object data) {
        return createMessage(CommonMessageType.SUCCESS, data);
    }
}
