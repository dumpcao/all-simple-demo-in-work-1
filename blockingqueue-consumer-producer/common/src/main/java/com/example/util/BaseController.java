package com.example.util;


import com.example.message.IMessageType;
import com.example.message.Message;
import com.example.message.MessageFactory;

/**
 * 控制层基类
 * 
 * @author terminator
 *
 */
public class BaseController {

	public Message response(String code, Object data) {
		return MessageFactory.createMessage(code,null, data);
	}

	public Message response(String code){
		return MessageFactory.createMessage(code,null,null);
	}
	/**
	 * 
	 * <b>方法说明：</b>
	 * <ul>
	 * 重载响应方法
	 * </ul>
	 * @param messageType 消息类型接口
	 * @param data 消息内容
	 * @return Message 消息对象
	 */
	public Message response(IMessageType messageType, Object data) {
        Message message = MessageFactory.createMessage(messageType, data);
        return message;
	}
    public Message response(IMessageType messageType) {
        Message message = MessageFactory.createMessage(messageType);
        return message;
    }

	public <T> Message<T> response(String code, String message, T data) {
        return MessageFactory.createMessage(code,message, data);
	}

    public <T> Message<T>  successResponse(T data) {
        Message message = MessageFactory.createSuccessMsg(data);
        return message;
    }

    public static  <T> Message<T> successResponse() {
        Message message = MessageFactory.createSuccessMsg();
        return message;
    }
}
