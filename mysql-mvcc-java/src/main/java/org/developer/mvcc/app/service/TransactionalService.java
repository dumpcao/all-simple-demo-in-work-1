package org.developer.mvcc.app.service;


import org.developer.mvcc.app.service.vo.UserInfoEntity;

public interface TransactionalService {

    Integer beginTrx();

    String initUserInfoTableMetaData();

    void update(Integer trxId);

    UserInfoEntity select(Integer trxId);

    void commit(Integer trxId);
}
