package org.developer.mvcc.app.service.vo;

import lombok.Data;

import java.util.List;

@Data
public class TableVO {
    private Integer tableId;

    private List<DataPageVO> dataPages;
}
