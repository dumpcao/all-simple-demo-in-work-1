package org.developer.mvcc.app.service.vo;

import lombok.Data;

import java.util.List;

@Data
public class DataPageVO {
    /**
     * 本数据页归属的表
     */
    private TableVO tableVO;

    /**
     * 表空间内的偏移量
     */
    private Integer offset;

    /**
     * 页内的记录
     */
    private List<TableRecordVO> recordVOList;
}
