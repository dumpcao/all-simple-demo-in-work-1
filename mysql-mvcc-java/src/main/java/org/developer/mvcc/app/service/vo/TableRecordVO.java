package org.developer.mvcc.app.service.vo;

import lombok.Data;

import java.util.List;

@Data
public class TableRecordVO {
    /**
     * 主键
     */
    private Integer recordId;

    /**
     * 数据
     */
    private UserInfoEntity content;
}
