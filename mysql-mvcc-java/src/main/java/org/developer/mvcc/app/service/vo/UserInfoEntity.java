package org.developer.mvcc.app.service.vo;

import lombok.Data;

@Data
public class UserInfoEntity {
    private Integer id;

    private String name;

    private Integer trxId;

    /**
     * 指向上一条记录的回滚日志
     */
    UndoLogVO undoLogVO;
}
