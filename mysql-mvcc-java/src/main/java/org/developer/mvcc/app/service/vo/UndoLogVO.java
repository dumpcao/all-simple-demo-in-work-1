package org.developer.mvcc.app.service.vo;

import lombok.Data;

@Data
public class UndoLogVO {
    private UndoLogVO lastUndoLog;

    private Integer trxId;

    private UserInfoEntity contentOfTheTrx;


}
