package org.developer.mvcc.app.service.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class ReadViewVO {
    /**
     * 视图创建时，活跃的事务集合
     */
    private List<Integer> activeTrxIdSet = new ArrayList<>();

    /**
     * 活跃事务数组中的最小值
     */
    private Integer lowLimit;

    /**
     * 待分配的事务id，等于当前已经分配的事务的id + 1
     */
    private Integer highLimit;

    /**
     * 视图创建者，如果某个版本的数据的trxId等于视图创建者，那么，对于该创建者事务来说，肯定是可见的
     */
    private Integer creatorTrxId;
}
