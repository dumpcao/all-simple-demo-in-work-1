package org.developer.mvcc.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.developer.mvcc.app.service.TransactionalService;
import org.developer.mvcc.app.service.vo.UserInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class demoController {

    @Autowired
    private TransactionalService transactionalService;

    @GetMapping("/beginTrx")
    public Integer beginTrx(){
        return transactionalService.beginTrx();
    }

    @GetMapping("/update")
    public void update(@RequestParam("trxId")Integer trxId){
        transactionalService.update(trxId);
    }

    @GetMapping("/commit")
    public void commit(@RequestParam("trxId")Integer trxId){
        transactionalService.commit(trxId);
    }

    @GetMapping("/select")
    public String select(@RequestParam("trxId")Integer trxId){
        UserInfoEntity s = transactionalService.select(trxId);

        return JSONObject.toJSONString(s, SerializerFeature.PrettyFormat);
    }

    @GetMapping("/initUserInfoTableMetaData")
    public String initUserInfoTableMetaData(){
        return transactionalService.initUserInfoTableMetaData();
    }



}
