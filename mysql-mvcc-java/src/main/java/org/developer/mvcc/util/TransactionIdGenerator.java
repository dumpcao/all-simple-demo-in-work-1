package org.developer.mvcc.util;

import java.util.concurrent.atomic.AtomicInteger;

public class TransactionIdGenerator {
    private static AtomicInteger trxId = new AtomicInteger(0);

    public static Integer nextTrxId() {
        return trxId.addAndGet(1);
    }

    public static Integer max() {
        return trxId.get();
    }
}
