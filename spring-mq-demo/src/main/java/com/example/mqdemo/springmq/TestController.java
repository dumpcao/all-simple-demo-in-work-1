package com.example.mqdemo.springmq;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class TestController {
    @Autowired
    AmqpTemplate template;


    @RequestMapping("/")
    public void test() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "ckl");
        map.put("age",20);

        String string = JSONObject.toJSONString(map);
        template.convertAndSend("CAD_1234", string);
    }
}
