package com.example.mqdemo.basicconsume;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.util.HashMap;
import java.util.Map;

import static com.example.mqdemo.AmqpClientTest.QUEUE_NAME;

public class ReceiveLogsTopic {

    private static final String EXCHANGE_NAME = "CAD-PUSH-TEST";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost("10.15.10.71");
        factory.setUsername("admin");
        factory.setPassword("admin");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic",true);
        channel.basicQos(2);

        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("x-single-active-consumer", true);
        arguments.put("x-queue-type", "classic");

        String queueName = channel.queueDeclare("CAD_1234",
                true, false, false, arguments).getQueue();


        channel.queueBind(queueName, EXCHANGE_NAME, "*.1234");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            long deliveryTag = delivery.getEnvelope().getDeliveryTag();

            System.out.println("delivertag:" + deliveryTag);
            channel.basicAck(deliveryTag, false);
        };
        channel.basicConsume("CAD_1234", false, deliverCallback, consumerTag -> { });



    }
}
