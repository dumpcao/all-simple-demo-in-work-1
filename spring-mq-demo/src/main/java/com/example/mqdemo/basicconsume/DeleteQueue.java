package com.example.mqdemo.basicconsume;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class DeleteQueue {

    private static final String EXCHANGE_NAME = "topic_logs";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost("localhost");
        factory.setUsername("admin");
        factory.setPassword("admin");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            /**
             * 因为队列不是exclusive的，所以我们可以删掉；
             * 删掉后，consumer那边会收到cancel notification
             */
            channel.queueDelete("ckl-test-consume");

        }
    }
}
