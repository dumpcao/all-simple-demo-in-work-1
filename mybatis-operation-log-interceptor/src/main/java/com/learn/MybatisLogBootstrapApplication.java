package com.learn;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
@Slf4j
@MapperScan("com.learn.mapper")
public class MybatisLogBootstrapApplication {



    public static void main(String[] args)  {
        ConfigurableApplicationContext application = SpringApplication.run(MybatisLogBootstrapApplication.class, args);
    }

}
