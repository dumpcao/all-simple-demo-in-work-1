package com.learn.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.learn.entity.OperationLog;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * operation_log Mapper 接口
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-10-28
 */
public interface OperationLogMapper extends BaseMapper<OperationLog> {

    @Select("select * from ${tableName} where ${whereSql}")
    List<Map<String, Object>> queryByWhereSql(@Param("tableName") String tableName, @Param("whereSql") String whereSql);


}
