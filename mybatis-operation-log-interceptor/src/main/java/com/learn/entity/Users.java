package com.learn.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.learn.annotation.EnableOperationLog;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-10-30
 * 该文件由ftl模板生成，如需批量修改，可直接修改模板后重新生成，
 * 参考module：mybatis_plus_generator
 */
@Data
@Accessors(chain = true)
@EnableOperationLog
public class Users {



    /**
     * 用户Id
     */
    @TableId(value = "user_id", type = IdType.ID_WORKER)
    private Long userId;




    /**
     * 用户名称
     */
    private String userName;




}
