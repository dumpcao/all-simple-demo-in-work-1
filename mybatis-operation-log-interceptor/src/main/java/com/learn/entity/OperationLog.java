package com.learn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * operation_log
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-10-30
 * 该文件由ftl模板生成，如需批量修改，可直接修改模板后重新生成，
 * 参考module：mybatis_plus_generator
 */
@Data
@Accessors(chain = true)
public class OperationLog {



    @TableId(value = "log_id", type = IdType.ID_WORKER)
    private Long logId;


    private String methodName;


    private Long operateId;


    private String operateName;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operateTime;


    private String operateType;

    private Integer operateTypeId;


    private String recordId;


    private String remark;


    private String tabName;


}
