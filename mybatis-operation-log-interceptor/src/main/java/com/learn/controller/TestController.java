// java
package com.learn.controller;

import com.learn.service.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * desc:
 * 测试接口，模拟从redis中获取缓存。当然，实际场景下，看缓存可以直接用工具的，这里就是举个栗子
 * @author : xxx
 * creat_date: 2019/6/18 0018
 * creat_time: 10:13
 **/
@RestController
public class TestController {

    @Autowired
    private IUsersService iUsersService;

    @RequestMapping("save.do")
    public String save(){
        iUsersService.saveUser();

        return "success";
    }


    @RequestMapping("update.do")
    public String update(@RequestParam("userId") long userId){
        iUsersService.updateUser(userId);

        return "success";
    }
}
