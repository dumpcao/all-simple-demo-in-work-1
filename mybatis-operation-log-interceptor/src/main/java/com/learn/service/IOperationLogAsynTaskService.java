package com.learn.service;


import com.learn.entity.OperationLog;

public interface IOperationLogAsynTaskService {
    /**
     * 添加日志到队列
     * @param operationLog
     */
    public void add(OperationLog operationLog);

    /**
     * 取出日志
     * @return
     */
    public OperationLog poll();

    /**
     * 初始异步日志任务
     */
    public void init();
}
