package com.learn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.learn.entity.OperationLog;
import com.learn.entity.Users;

import java.util.List;
import java.util.Map;

public interface IOperationLogService extends IService<OperationLog> {


    /**
     * 通过表名和条件查询数据：用于记录日志时查询数据
     *
     * @param tableName
     * @param whereSql
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    List<Map<String, Object>> queryByWhereSql(String tableName, String whereSql);
}
