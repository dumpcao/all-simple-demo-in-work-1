package com.learn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.learn.entity.Users;

public interface IUsersService extends IService<Users> {


    void saveUser();

    void updateUser(long userId);

}
