package com.learn.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.learn.entity.OperationLog;
import com.learn.entity.Users;
import com.learn.mapper.OperationLogMapper;
import com.learn.mapper.UsersMapper;
import com.learn.service.IOperationLogService;
import com.learn.service.IUsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class IOperationLogkServiceImpl extends ServiceImpl<OperationLogMapper, OperationLog> implements IOperationLogService {
    /**
     * 通过表名和条件查询数据：用于记录日志时查询数据
     *
     * @param tableName
     * @param whereSql
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    @Override
    public List<Map<String, Object>> queryByWhereSql(String tableName, String whereSql) {
        List<Map<String, Object>> result = this.baseMapper.queryByWhereSql(tableName, whereSql);
        return result;
    }

}
