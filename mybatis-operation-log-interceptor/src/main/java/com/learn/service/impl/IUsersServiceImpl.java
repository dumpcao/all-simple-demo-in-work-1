package com.learn.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.learn.entity.Users;
import com.learn.mapper.UsersMapper;
import com.learn.service.IUsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class IUsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements IUsersService {

    @Override
    public void saveUser() {
        Users users = new Users();
        users.setUserId(IdWorker.getId());
        users.setUserName("xxxxx");

        this.save(users);
    }

    @Override
    public void updateUser(long userId) {
        Users users = this.getById(userId);
        users.setUserName("222");

        this.updateById(users);
    }
}
