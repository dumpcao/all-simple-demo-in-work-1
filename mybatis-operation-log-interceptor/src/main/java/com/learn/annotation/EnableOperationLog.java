package com.learn.annotation;

import java.lang.annotation.*;

/**
 * 记录系统操作日志的注解
 *
 * @Author: xxx
 * @CreateDate: 10:09 2019/11/18/018
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnableOperationLog {

}
