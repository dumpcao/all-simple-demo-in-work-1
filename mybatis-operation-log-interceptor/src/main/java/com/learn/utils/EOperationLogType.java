package com.learn.utils;

public enum EOperationLogType {

    ADD(1,"add"),
    UPDATE(2,"update"),
    DELETE(3, "delete");

    private int value;
    private String des;

    EOperationLogType(int value, String des){
        this.value = value;
        this.des = des;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
