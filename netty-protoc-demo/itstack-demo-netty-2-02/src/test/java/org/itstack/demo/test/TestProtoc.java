package org.itstack.demo.test;

import com.example.tutorial.AddressBookProtos;

public class TestProtoc {
    public static void main(String[] args) {
        AddressBookProtos.Person.Builder personBuilder = AddressBookProtos.Person.newBuilder();
        personBuilder.setId(111).setEmail("ajsjk@163.com");

        AddressBookProtos.Person.PhoneNumber.Builder phoneNumberBuilder = AddressBookProtos.Person.PhoneNumber.newBuilder();
        phoneNumberBuilder.setNumber("222");
        phoneNumberBuilder.setType(AddressBookProtos.Person.PhoneType.MOBILE);

        personBuilder.addPhones(phoneNumberBuilder.build());
        AddressBookProtos.AddressBook.Builder builder = AddressBookProtos.AddressBook.newBuilder();
        builder.addPeople(personBuilder.build());

        AddressBookProtos.AddressBook addressBook = builder.build();
        System.out.println(addressBook);
        addressBook.toByteArray();
    }
}
