package com.ceiec;


/**
 * 实现B+树
 *
 * @param <V> 指定值类型
 * @param <K> 使用泛型，指定索引类型,并且指定必须继承Comparable
 */
public class BPlusTree <V, K extends Comparable<K>>{
    //B+树的阶
    private Integer bTreeOrder;
    //B+树的非叶子节点最小拥有的子节点数量（同时也是键的最小数量）
    //private Integer minNUmber;
    //B+树的非叶子节点最大拥有的节点数量（同时也是键的最大数量）
    private Integer maxNumber;

    private Node<V, K> root;

    private LeafNode<V, K> left;

    //无参构造方法，默认阶为3
    public BPlusTree(){
        this(3);
    }

    //有参构造方法，可以设定B+树的阶
    public BPlusTree(Integer bTreeOrder){
        this.bTreeOrder = bTreeOrder;
        //this.minNUmber = (int) Math.ceil(1.0 * bTreeOrder / 2.0);
        //因为插入节点过程中可能出现超过上限的情况,所以这里要加1
        this.maxNumber = bTreeOrder + 1;
        this.root = new LeafNode<V, K>();
        this.left = null;
    }

    //查询
    public V find(K key){
        V t = this.root.find(key);
        if(t == null){
            System.out.println("不存在");
        }
        return t;
    }

    //插入
    public void insert( K key,V value){
        if(key == null) {
            return;
        }
        Node<V, K> t = this.root.insert(value, key);
        if(t != null) {
            this.root = t;
        }
        this.left = (LeafNode<V, K>)this.root.refreshLeft();

//        System.out.println("插入完成,当前根节点为:");
//        for(int j = 0; j < this.root.number; j++) {
//            System.out.print((K) this.root.keys[j] + " ");
//        }
//        System.out.println();
    }


    /**
     * 节点父类，因为在B+树中，非叶子节点不用存储具体的数据，只需要把索引作为键就可以了
     * 所以叶子节点和非叶子节点的类不太一样，但是又会公用一些方法，所以用Node类作为父类,
     * 而且因为要互相调用一些公有方法，所以使用抽象类
     *
     * @param <V> 同BPlusTree
     * @param <K>
     */
    abstract class Node<V, K extends Comparable<K>>{
        //父节点
        protected Node<V, K> parent;
        //子节点
        protected Node<V, K>[] childs;
        //键（子节点）数量
        protected Integer number;
        //键
        protected Object keys[];

        //构造方法
        public Node(){
            this.keys = new Object[maxNumber];
            this.childs = new Node[maxNumber];
            this.number = 0;
            this.parent = null;
        }

        //查找
        abstract V find(K key);

        //插入
        abstract Node<V, K> insert(V value, K key);

        abstract LeafNode<V, K> refreshLeft();
    }


    /**
     * 非叶节点类
     * @param <V>
     * @param <K>
     */

    class IndexNodeNotLeaf<V, K extends Comparable<K>> extends Node<V, K>{

        public IndexNodeNotLeaf() {
            super();
        }

        /**
         * 递归查找,这里只是为了确定值究竟在哪一块,真正的查找到叶子节点才会查
         * @param key
         * @return
         */
        @Override
        V find(K key) {
            int i = 0;
            while(i < this.number){
                if(key.compareTo((K) this.keys[i]) <= 0) {
                    break;
                }
                i++;
            }
            if(this.number == i) {
                return null;
            }
            return this.childs[i].find(key);
        }

        /**
         * 递归插入,先把值插入到对应的叶子节点,最终讲调用叶子节点的插入类
         * @param value
         * @param key
         */
        @Override
        Node<V, K> insert(V value, K key) {
            int i = 0;
            while(i < this.number){
                if(key.compareTo((K) this.keys[i]) < 0) {
                    break;
                }
                i++;
            }
            if(key.compareTo((K) this.keys[this.number - 1]) >= 0) {
                i--;
//                if(this.childs[i].number + 1 <= bTreeOrder) {
//                    this.keys[this.number - 1] = key;
//                }
            }

//            System.out.println("非叶子节点查找key: " + this.keys[i]);

            return this.childs[i].insert(value, key);
        }

        @Override
        LeafNode<V, K> refreshLeft() {
            return this.childs[0].refreshLeft();
        }

        /**
         * 插入时，先在某个叶子节点上操作，叶子节点最大的那个key，即这里的参数key，会被传进这个函数
         * 因为，这个key，在上层parent中的keys中是存储了的
         * parent可能是有个key，所以需要根据参数key，找到触发这次操作的那个叶子节点的位置。
         *
         * 当叶子节点插入成功完成分解时,递归地向父节点插入新的节点以保持平衡
         * @param node1
         * @param node2
         * @param key
         */
        Node<V, K> insertNode(Node<V, K> node1, Node<V, K> node2, K key){

//            System.out.println("非叶子节点,插入key: " + node1.keys[node1.number - 1] + " " + node2.keys[node2.number - 1]);

            // 获取原有的最大的key
            K oldKey = null;
            if(this.number > 0){
                oldKey = (K) this.keys[this.number - 1];
            }


            //如果原有key为null,说明这个非节点是空的,直接放入两个节点即可
            if(key == null || this.number <= 0){
//                System.out.println("非叶子节点,插入key: " + node1.keys[node1.number - 1] + " " + node2.keys[node2.number - 1] + "直接插入");
                this.keys[0] = node1.keys[node1.number - 1];
                this.keys[1] = node2.keys[node2.number - 1];
                this.childs[0] = node1;
                this.childs[1] = node2;
                this.number += 2;
                return this;
            }
            //原有节点不为空,则应该先寻找原有节点的位置,然后将新的节点插入到原有节点中
            int i = 0;
            while(key.compareTo((K)this.keys[i]) != 0){
                i++;
            }

            // 现在这个parent本身已经有keys数组了，比如，原keys数组为3 8 10 24,要插入的为node1，就是10那个子节点；
            // 16，是要新插入的右孩子节点， 最终要变成：
            // 3 8 10 16 24
            this.keys[i] = node1.keys[node1.number - 1];
            this.childs[i] = node1;

            Object tempKeys[] = new Object[maxNumber];
            Object tempChilds[] = new Node[maxNumber];

            System.arraycopy(this.keys, 0, tempKeys, 0, i + 1);
            System.arraycopy(this.childs, 0, tempChilds, 0, i + 1);
            System.arraycopy(this.keys, i + 1, tempKeys, i + 2, this.number - i - 1);
            System.arraycopy(this.childs, i + 1, tempChilds, i + 2, this.number - i - 1);
            tempKeys[i + 1] = node2.keys[node2.number - 1];
            tempChilds[i + 1] = node2;

            this.number++;

            //判断是否需要拆分
            //如果不需要拆分,把数组复制回去,直接返回
            if(this.number <= bTreeOrder){
                System.arraycopy(tempKeys, 0, this.keys, 0, this.number);
                System.arraycopy(tempChilds, 0, this.childs, 0, this.number);

//                System.out.println("非叶子节点,插入key: " + node1.keys[node1.number - 1] + " " + node2.keys[node2.number - 1] + ", 不需要拆分");

                return null;
            }

//            System.out.println("非叶子节点,插入key: " + node1.keys[node1.number - 1] + " " + node2.keys[node2.number - 1] + ",需要拆分");

            //如果需要拆分,和拆叶子节点时类似,从中间拆开
            Integer middle = this.number / 2;

            //新建非叶子节点,作为拆分的右半部分
            IndexNodeNotLeaf<V, K> tempNode = new IndexNodeNotLeaf<V, K>();
            //非叶节点拆分后应该将其子节点的父节点指针更新为正确的指针
            tempNode.number = this.number - middle;
            tempNode.parent = this.parent;
            //如果父节点为空,则新建一个非叶子节点作为父节点,并且让拆分成功的两个非叶子节点的指针指向父节点
            if(this.parent == null) {

//                System.out.println("非叶子节点,插入key: " + node1.keys[node1.number - 1] + " " + node2.keys[node2.number - 1] + ",新建父节点");

                IndexNodeNotLeaf<V, K> tempIndexNodeNotLeaf = new IndexNodeNotLeaf<>();
                tempNode.parent = tempIndexNodeNotLeaf;
                this.parent = tempIndexNodeNotLeaf;
                oldKey = null;
            }
            System.arraycopy(tempKeys, middle, tempNode.keys, 0, tempNode.number);
            System.arraycopy(tempChilds, middle, tempNode.childs, 0, tempNode.number);
            for(int j = 0; j < tempNode.number; j++){
                tempNode.childs[j].parent = tempNode;
            }
            //让原有非叶子节点作为左边节点
            this.number = middle;
            this.keys = new Object[maxNumber];
            this.childs = new Node[maxNumber];
            System.arraycopy(tempKeys, 0, this.keys, 0, middle);
            System.arraycopy(tempChilds, 0, this.childs, 0, middle);

            //叶子节点拆分成功后,需要把新生成的节点插入父节点
            IndexNodeNotLeaf<V, K> parentNode = (IndexNodeNotLeaf<V, K>)this.parent;
            return parentNode.insertNode(this, tempNode, oldKey);
        }

    }

    /**
     * 叶节点类
     * @param <V>
     * @param <K>
     */
    class LeafNode <V, K extends Comparable<K>> extends Node<V, K> {

        protected Object values[];
        protected LeafNode left;
        protected LeafNode right;

        public LeafNode(){
            super();
            this.values = new Object[maxNumber];
            this.left = null;
            this.right = null;
        }

        /**
         * 进行查找,经典二分查找,不多加注释
         * @param key
         * @return
         */
        @Override
        V find(K key) {
            if(this.number <=0) {
                return null;
            }

//            System.out.println("叶子节点查找");

            Integer left = 0;
            Integer right = this.number;

            Integer middle = (left + right) / 2;

            while(left < right){
                K middleKey = (K) this.keys[middle];
                if(key.compareTo(middleKey) == 0) {
                    return (V) this.values[middle];
                } else if(key.compareTo(middleKey) < 0) {
                    right = middle;
                } else {
                    left = middle;
                }
                middle = (left + right) / 2;
            }
            return null;
        }

        /**
         *
         * @param value
         * @param key
         */
        @Override
        Node<V, K> insert(V value, K key) {

//            System.out.println("叶子节点,插入key: " + key);

            //保存原始存在父节点的key值
            K oldKey = null;
            if(this.number > 0) {
                oldKey = (K) this.keys[this.number - 1];
            }
            //先插入数据
            int i = 0;
            while(i < this.number){
                // 和这个叶子节点里面的每个key进行比较，如果比这个key小，说明需要在这个key的左边插入，结束
                if(key.compareTo((K) this.keys[i]) < 0) {
                    break;
                }
                i++;
            }

            //复制数组,完成添加
            Object[] targetKeys = getTargetKeyArrayAfterInsert(key, i);
            Object[] targetValues = getTargetValueArrayAfterInsert(value, i);

            this.number++;

//            System.out.println("插入完成,当前节点key为:");
//            for(int j = 0; j < this.number; j++)
//                System.out.print(targetKeys[j] + " ");
//            System.out.println();

            //判断是否需要拆分
            //如果不需要拆分完成复制后直接返回
            if(this.number <= bTreeOrder){
                System.out.println("叶子节点,插入key: " + key + ",不需要拆分");
                nodeNoNeedSplit(targetKeys, targetValues);
                return null;
            }

            System.out.println("叶子节点,插入key: " + key + ",需要拆分");

            //如果需要拆分,则从中间把节点拆分差不多的两部分
            Integer middle = this.number / 2;

            //新建叶子节点,作为拆分的右半部分
            LeafNode<V, K> rightNodeAfterSplit = new LeafNode<V, K>();
            rightNodeAfterSplit.number = this.number - middle;
            rightNodeAfterSplit.parent = this.parent;
            //如果父节点为空,则新建一个非叶子节点作为父节点,并且让拆分成功的两个叶子节点的指针指向父节点
            if(this.parent == null) {

                System.out.println("叶子节点,插入key: " + key + ",父节点为空 新建父节点");

                IndexNodeNotLeaf<V, K> tempIndexNodeNotLeaf = new IndexNodeNotLeaf<>();
                rightNodeAfterSplit.parent = tempIndexNodeNotLeaf;
                this.parent = tempIndexNodeNotLeaf;
                oldKey = null;
            }
            System.arraycopy(targetKeys, middle, rightNodeAfterSplit.keys, 0, rightNodeAfterSplit.number);
            System.arraycopy(targetValues, middle, rightNodeAfterSplit.values, 0, rightNodeAfterSplit.number);

            //让原有叶子节点作为拆分的左半部分
            this.number = middle;
            this.keys = new Object[maxNumber];
            this.values = new Object[maxNumber];
            System.arraycopy(targetKeys, 0, this.keys, 0, middle);
            System.arraycopy(targetValues, 0, this.values, 0, middle);

            this.right = rightNodeAfterSplit;
            rightNodeAfterSplit.left = this;

            //叶子节点拆分成功后,需要把新生成的节点插入父节点
            IndexNodeNotLeaf<V, K> parentNode = (IndexNodeNotLeaf<V, K>)this.parent;
            return parentNode.insertNode(this, rightNodeAfterSplit, oldKey);
        }

        private void nodeNoNeedSplit(Object[] targetKeys, Object[] targetValues) {
            System.arraycopy(targetKeys, 0, this.keys, 0, this.number);
            System.arraycopy(targetValues, 0, this.values, 0, this.number);

            //有可能虽然没有节点分裂，但是实际上插入的值大于了原来的最大值，所以所有父节点的边界值都要进行更新
            Node node = this;
            while (node.parent != null){
                /**
                 * 获取当前节点，最右边的那个key，也就是边界值，右边界是最大的那个
                 */
                K tempkey = (K)node.keys[node.number - 1];
                /**
                 * 如果大于parent的边界值
                 */
                K biggestKeyOfParent = (K) node.parent.keys[node.parent.number - 1];
                if(tempkey.compareTo(biggestKeyOfParent) > 0){
                    node.parent.keys[node.parent.number - 1] = tempkey;
                    node = node.parent;
                }
                else {
                    break;
                }
            }
        }

        private Object[] getTargetValueArrayAfterInsert(V value, int i) {
            Object targetValues[] = new Object[maxNumber];
            /**
             * 上面的while，找到节点要插入的地方后，相当于，i把这个节点，分成了两半。
             * 现在就是，把小于i的这一侧的数据，拷贝到target数组
             */
            System.arraycopy(this.values, 0, targetValues, 0, i);
            /**
             * target数组的i这个空，要留给我们待插入的元素
             */
            targetValues[i] = value;

            /**
             * 把原数组的右侧的数据，拷贝到target，target要从i+1开始。
             */
            System.arraycopy(this.values, i, targetValues, i + 1, this.number - i);

            return targetValues;
        }

        private Object[] getTargetKeyArrayAfterInsert(K key, int i) {
            Object targetKeys[] = new Object[maxNumber];
            /**
             * 上面的while，找到节点要插入的地方后，相当于，i把这个节点，分成了两半。
             * 现在就是，把小于i的这一侧的数据，拷贝到target数组
             */
            System.arraycopy(this.keys, 0, targetKeys, 0, i);
            /**
             * target数组的i这个空，要留给我们待插入的元素
             */
            targetKeys[i] = key;
            /**
             * 把原数组的右侧的数据，拷贝到target，target要从i+1开始。
             */
            System.arraycopy(this.keys, i, targetKeys, i + 1, this.number - i);
            return targetKeys;
        }

        @Override
        LeafNode<V, K> refreshLeft() {
            if(this.number <= 0) {
                return null;
            }
            return this;
        }
    }
}


