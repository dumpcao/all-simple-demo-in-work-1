package com.example.demo;

import com.example.bean.TestIdEntity;
import com.example.bean.TestIdPo;
import com.example.repo.TestMapper;
import com.example.repo.TestRepo;
import com.example.service.TestService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.events.Event.ID;

@RestController
public class TestController {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TestRepo testRepo;

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private TestService testService;

    @GetMapping("query")
    @ResponseBody
    public List<Map<String, Object>> query() {
        List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from test_id");
        return list;
    }

//    @GetMapping("queryJpa/{id}")
//    @ResponseBody
//    public TestIdEntity queryJpa(@PathVariable Integer id) {
//        Optional<TestIdEntity> entity = testRepo.findById(id);
//        TestIdEntity testIdEntity = entity.get();
//        return testIdEntity;
//    }

    @GetMapping("queryById/{id}")
    @ResponseBody
    public TestIdPo queryById(@PathVariable Integer id) {
//        TestIdPo entity = testMapper.findById(id);
        TestIdPo testIdPo = testService.findById(id);
        return testIdPo;
    }
}
