package com.example.service.impl;

import com.example.bean.TestIdPo;
import com.example.repo.TestMapper;
import com.example.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestMapper testMapper;

    @Override
    @Transactional
    public TestIdPo findById(Integer id) {
        TestIdPo testIdPo = testMapper.selectCityById(id);
        return testIdPo;
    }
}
