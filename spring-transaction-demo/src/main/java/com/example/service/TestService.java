package com.example.service;

import com.example.bean.TestIdPo;

public interface TestService {
    TestIdPo findById(Integer id);
}
