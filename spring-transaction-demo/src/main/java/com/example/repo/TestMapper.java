package com.example.repo;

import com.example.bean.TestIdEntity;
import com.example.bean.TestIdPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TestMapper {
    @Select("select * from test_id where id = #{id}")
    TestIdPo findById(@Param("id") Integer id);

    TestIdPo selectCityById(@Param("id") Integer id);
}
