package com.example.repo;

import com.example.bean.TestIdEntity;
import org.springframework.data.repository.CrudRepository;

public interface TestRepo extends CrudRepository<TestIdEntity,Integer> {

}
