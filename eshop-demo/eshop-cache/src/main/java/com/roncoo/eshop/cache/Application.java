package com.roncoo.eshop.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.HashSet;
import java.util.Set;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan
public class Application {

    
//    @SuppressWarnings({ "rawtypes", "unchecked" })
//	@Bean
//    public ServletListenerRegistrationBean servletListenerRegistrationBean() {
//    	ServletListenerRegistrationBean servletListenerRegistrationBean =
//    			new ServletListenerRegistrationBean();
////    	servletListenerRegistrationBean.setListener(new InitListener());
//    	return servletListenerRegistrationBean;
//    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
}