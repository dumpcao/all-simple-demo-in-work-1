package org.developer.velocity;

import org.apache.commons.lang.ArrayUtils;
import org.developer.velocity.app.service.FileReaderUtil;
import org.developer.velocity.app.service.MqMetadata;
import org.developer.velocity.util.GenerateCodeByVelocityUtil;
import org.springframework.util.StringUtils;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MqCodeTypeParser {

    enum ParserState{
        NONE,START_COMMENT,COMMENT_END,MQ_START,MQ_END
    }
    public static void main(String[] args) throws IOException {
        List<MqMetadata> list = parseMqFile2GetMetadataList();

        GenerateCodeByVelocityUtil.generate(list);

    }


    private static List<MqMetadata> parseMqFile2GetMetadataList() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL resource = loader.getResource("mqcode-type-1");
        BufferedInputStream content = (BufferedInputStream) resource.getContent();
        InputStreamReader reader = new InputStreamReader(content);
        // create a buffered reader to read commands from standard in
        BufferedReader in = new  BufferedReader(reader);

        // process commands until user quits
        ParserState parserState = ParserState.NONE;
        String[] cmd;

        List<MqMetadata> list = new ArrayList<>();
        MqMetadata currentMqMetadata = null;
        outerloop:
        while (true) {
            try {
                cmd = FileReaderUtil.readCommand(in);
                System.out.println("current:" + StringUtils.arrayToCommaDelimitedString(cmd));
            } catch (EOFException e) {
                break;
            }


            if (cmd != null) {
                for (String s : cmd) {
                    if (s.startsWith("/**")) {
                        parserState = ParserState.START_COMMENT;
                        continue outerloop;
                    }

                    if (parserState == ParserState.START_COMMENT) {
                        if (s.equals("*/")) {
                            parserState = ParserState.COMMENT_END;
                            continue outerloop;
                        }
                        if (s.startsWith("*")) {
                            /**
                             * 注释
                             */
                            Object[] subarray = ArrayUtils.subarray(cmd, 1, cmd.length);
                            currentMqMetadata = new MqMetadata();

                            String comment = StringUtils.arrayToDelimitedString(subarray, " ");
                            currentMqMetadata.setComment(comment);
                            continue outerloop;
                        }
                        throw new RuntimeException();
                    }

                    if (parserState == ParserState.COMMENT_END || s.startsWith("CODE")) {
                        if (currentMqMetadata == null) {
                            currentMqMetadata = new MqMetadata();
                        }
                        currentMqMetadata.setCodeAndDescOriginDataArray(cmd);
                        currentMqMetadata.setMqEnumName(cmd[0]);
                        currentMqMetadata.setMqCode(cmd[1]);
                        currentMqMetadata.setMqDesc(cmd[2]);
                        currentMqMetadata.setCodeAndDescOriginData(StringUtils.arrayToCommaDelimitedString(cmd));
                        if (currentMqMetadata.getCodeAndDescOriginData().contains("11007")) {
                            System.out.println("stop");
                        }

                        parserState = ParserState.MQ_START;
                        continue outerloop;
                    }

                    if (parserState == ParserState.MQ_START) {

                        currentMqMetadata.setMqBodyType(cmd[0]);
                        System.out.println(currentMqMetadata);
                        list.add(currentMqMetadata);

                        currentMqMetadata = null;
                    }
                }
                System.out.println("current line end\r\n");
            }
        }

        /**
         * 获取到了整个列表
         */
        System.out.println(list);

        return list;
    }


}
