package org.developer.velocity.app.service;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.util.StringTokenizer;

public class FileReaderUtil {






    public static String[] readCommand(BufferedReader in) throws IOException {
        int i = 0;
        String cmd[] = null;

        String cmdLine = in.readLine();

        if (cmdLine != null) {
//            StringTokenizer st = new StringTokenizer(cmdLine);
            StringTokenizer st = new StringTokenizer(cmdLine," \t\n\r\f(,", false);
            if (st.countTokens() != 0) {
                cmd = new String[st.countTokens()];
                while (st.hasMoreTokens()) {
                    cmd[i++] = st.nextToken();
                }
            }
        } else {
            throw new EOFException("no more,eof");
        }

        return cmd;
    }
}
