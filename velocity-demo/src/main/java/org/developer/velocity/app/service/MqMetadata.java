package org.developer.velocity.app.service;

import lombok.Data;

@Data
public class MqMetadata {
    private String[] codeAndDescOriginDataArray;

    private String codeAndDescOriginData;

    /**
     * mq消息体的class类型，如
     * GetPendingTransferListWaitingToApproveRespVO
     *
     */
    private String mqBodyType;

    String comment;

    /**
     * mq的枚举名称，如：
     * CODE_13000_NOTICE_PRIVILEGE_PERSON
     */
    private String mqEnumName;

    /**
     * mq的code，如13000
     */
    private String mqCode;


    /**
     * mq的描述
     */
    private String mqDesc;

}
