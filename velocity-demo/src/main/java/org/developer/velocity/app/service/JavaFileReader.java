package org.developer.velocity.app.service;

;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class JavaFileReader {

    enum ParserState {
        NONE, IMPORT_STARTED,CLASS_START, START_COMMENT, COMMENT_END, FIELD_START, FIELD_END
    }


    public static void main(String[] args) throws IOException {
        ClassPathResource resource = new ClassPathResource("javafilelist");
        String s = StreamUtils.copyToString(resource.getInputStream(), Charset.forName("utf-8"));
        String[] lines = s.split("\r\n");

//            processOneFile("");
        int sum = 0;
        for (String filePath : lines) {
            boolean b = processOneFile(filePath);
            if (b) {
                sum++;
            }
        }

        System.out.println("process " + sum + " files");

    }

    private static boolean processOneFile(String filePath) throws IOException {
//        File file = new File("F:\\workproject_codes\\cad3.0-for-2018-idea-use\\CAD_VO\\src\\main\\java\\com\\ceiec\\cad\\service\\mqmessage\\NoticeMqMessageVo.java");
        File file = new File(filePath);

        InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
        BufferedReader in = new BufferedReader(reader);

        ParserState parserState = ParserState.NONE;


        List<String> allLines = new ArrayList<>();
        while (true) {
            try {
                String line = in.readLine();
                if (null == line) {
                    break;
                }
                allLines.add(line);
            } catch (EOFException e) {
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //检查是否已经处理过了
        for (String allLine : allLines) {
            if (allLine.contains("ApiModel") || allLine.contains("ApiModelProperty")) {
                System.out.println("process " + filePath + "end ,no need");
                return false;
            }
        }


        ArrayList<String> finalList = new ArrayList<>();
        String fieldComment = null;
        for (int i = 0; i < allLines.size(); i++) {
            String line = allLines.get(i);

            while (true) {
                String trimmedLine = line.trim();
                if (trimmedLine.contains(" class ") && trimmedLine.contains("public")) {
                    parserState = ParserState.CLASS_START;
                    finalList.add("@ApiModel");
                    break;
                }

                if (org.apache.commons.lang.StringUtils.isBlank(trimmedLine)) {
                    break;
                }

                if (trimmedLine.contains("import") && parserState == ParserState.NONE) {
                    parserState = ParserState.IMPORT_STARTED;
                    finalList.add("import io.swagger.annotations.ApiModel;");
                    finalList.add("import io.swagger.annotations.ApiModelProperty;");
                    break;
                }

                if (parserState == ParserState.NONE) {
                    break;
                }
                System.out.println("current line:" + i + ", line :" + line);
                if (trimmedLine.startsWith("/**")) {
                    if (trimmedLine.endsWith("*/")) {
                        if (fieldComment == null) {
                            fieldComment = trimmedLine.replace("/**", "").replace("*/", "");
                        }
                        parserState = ParserState.COMMENT_END;
                    } else {
                        parserState = ParserState.START_COMMENT;
                    }
                    break;
                }

                if (parserState == ParserState.START_COMMENT) {
                    if (trimmedLine.equals("*/") || trimmedLine.equals("**/")) {
                        parserState = ParserState.COMMENT_END;
                        break;
                    }
                    if (StringUtils.isEmpty(trimmedLine)) {
                        break;
                    }
                    if (trimmedLine.startsWith("*")) {
                        /**
                         * 注释
                         */
                        if (fieldComment == null) {
                            fieldComment = trimmedLine;
                        } else {
                            fieldComment = fieldComment + ", " + trimmedLine;
                        }
                        break;
                    }

                    throw new RuntimeException();
                }

                if (parserState == ParserState.COMMENT_END) {
                    if (StringUtils.isEmpty(trimmedLine)) {
                        break;
                    }

                    /**
                     * 这是个属性
                     */
                    if ((trimmedLine.startsWith("private") || trimmedLine.startsWith("public")) && trimmedLine.endsWith(";")) {
                        if (!StringUtils.isEmpty(fieldComment)) {
                            finalList.add("    @ApiModelProperty(value = \"" + fieldComment.replace("*","") + "\")");
                        } else {
                            System.out.println("no comment");
                        }

                        /**
                         * clear
                         */
                        fieldComment = null;
                        break;
                    }
                }

                // 不需要处理的行
                break;
            }// end while

            finalList.add(line);

        }
        System.out.println("result ---------------------------------------------- ");
        for (String s : finalList) {
            System.out.println(s);
        }


//        File fileForOutput = new File("F:\\workproject_codes\\cad3.0-for-2018-idea-use\\CAD_VO\\src\\main\\java\\com\\ceiec\\cad\\service\\mqmessage\\I18nInformSubMsg.java.bak");
        File fileForOutput = file;
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileForOutput));
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream));
        for (String s : finalList) {
            writer.write(s);
            writer.newLine();
        }
        writer.flush();
        System.out.println("process " + filePath + "end ");
        return true;
    }
}
