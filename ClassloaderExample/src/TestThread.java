/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2019/6/26 0026
 * creat_time: 9:01
 **/
public class TestThread {
    public static void main(String[] args) {
        Thread.currentThread().setContextClassLoader(TestThread.class.getClassLoader().getParent());
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
                System.out.println(Thread.currentThread().getContextClassLoader());
            }
        }).start();

        System.out.println("main " + Thread.currentThread().getContextClassLoader());
    }
}
