package classloader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class CalculatorTest {

	public static void main(String[] args) throws MalformedURLException {
        File file = new File("C:\\Users\\Gaoyu\\Documents\\WeChat Files\\qiyepretty\\FileStorage\\File\\2019-06");
        URI uri = file.toURI();
        URL toURL = uri.toURL();
        System.out.println(toURL);
        String url = "http://localhost:8080/";
		NetworkClassLoader ncl = new NetworkClassLoader(url);
		String basicClassName = "com.example.CalculatorBasic";
		String advancedClassName = "com.example.CalculatorAdvanced";
		try {
			Class<?> clazz = ncl.loadClass(basicClassName);
			ICalculator calculator = (ICalculator) clazz.newInstance();
			System.out.println(calculator.getVersion());
			clazz = ncl.loadClass(advancedClassName);
			calculator = (ICalculator) clazz.newInstance();
			System.out.println(calculator.getVersion());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
