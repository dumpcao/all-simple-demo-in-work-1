# 介绍
使用jsoup读取博客园的相关分类及分类下文章，然后生成markdown格式的文档，最终效果见：
https://www.cnblogs.com/grey-wolf/p/12934232.html

中间使用了mappedByteBuffer来实现缓存，避免每次都去博客园爬取相关数据，主要是为了熟练相关api的使用。
使用普通io流也可以的，不存文件，也可以的。
主要还是熟悉下nio的api