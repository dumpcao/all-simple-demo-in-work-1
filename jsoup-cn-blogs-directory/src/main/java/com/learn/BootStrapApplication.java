package com.learn;


import com.learn.model.CategoryDomain;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@ComponentScan(value = "com.learn")
@Configuration
public class BootStrapApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BootStrapApplication.class);
        CategoryProcessServcie categoryProcessServcie = context.getBean(CategoryProcessServcie.class);
        /**
         * 获取目录列表,生成目录文件
         * 位置在当前idea工程的根路径下
         */
        List<CategoryDomain> catagoryListFromCnblogs = categoryProcessServcie.getCategoryListFromCnblogs();

        /**
         * 生成每个类别的文件
         */
        BlogListService blogListService = context.getBean(BlogListService.class);
        blogListService.generateBlogListOfAllCategory(catagoryListFromCnblogs);

        /**
         * 生成markdown格式的字符串
         */
        blogListService.generateMarkdownFormat();

    }

}
