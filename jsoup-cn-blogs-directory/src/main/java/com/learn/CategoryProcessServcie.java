package com.learn;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.learn.model.CategoryDomain;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static sun.util.logging.LoggingSupport.log;


@Slf4j
@Component
public class CategoryProcessServcie {

    Pattern pattern = Pattern.compile("(.*)\\((\\d+)\\)");

    private static final String CATEGORY_FILE_NAME = "category.json";


    public List<CategoryDomain> getCategoryListFromCnblogs() {
        /**
         * 读取本地文件缓存
         */
        List<CategoryDomain> categoryDomains = MappedFileUtil.readFromCachedFile(CategoryDomain.class, CATEGORY_FILE_NAME);
        if (!CollectionUtils.isEmpty(categoryDomains)) {
            return categoryDomains;
        }

        /**
         * 如果本地文件没有读到，则去网络请求，并写入本地文件
         */
        Document doc = null;
        try {
            doc = Jsoup.connect("https://www.cnblogs.com/grey-wolf/ajax/sidecolumn.aspx").get();
        } catch (IOException e) {
            log.error("e:{}", e);
            return null;
        }

        Elements elements = doc.select("#sidebar_postcategory a");
        if (CollectionUtils.isEmpty(elements)) {
            return new ArrayList<>();
        }

        /**
         * 转换为需要的vo
         */
        List<CategoryDomain> list = convert2CategoryDomains(elements);

        /**
         * 写入文件
         */
        MappedFileUtil.write2file(list,CATEGORY_FILE_NAME);

        return list;
    }

    private List<CategoryDomain> convert2CategoryDomains(Elements elements) {
        List<CategoryDomain> result = new ArrayList<CategoryDomain>();
        for (Element element : elements) {
            CategoryDomain categoryDomain = new CategoryDomain();
            /**
             * 获取类别的链接
             */
            String href = element.attr("href");
            categoryDomain.setHref(href);

            /**
             * 获取名称和数量，一般是这种格式
             *  DAO层框架(11)
             */
            TextNode node = (TextNode) element.childNode(0);
            String nameAndCount = node.toString().trim();
            Matcher matcher = pattern.matcher(nameAndCount);
            if (matcher.find()) {
                String name = matcher.group(1);
                String countStr = matcher.group(2);

                /**
                 * 其中一个分类叫做spring/boot源码解析，这个/有点问题，用这个名字，创建不了文件
                 */
                name = name.replaceAll("/"," ");
                /**
                 * 有个分类是这样的：Web&HTTP，也有问题
                 * &amp; 这个代表&, html中的转义符，对&转义
                 */
                name = name.replaceAll("&amp;"," ");


                categoryDomain.setCategoryChineseName(name);
                categoryDomain.setBlogCountOfTheCategory(Integer.valueOf(countStr));
            }

            result.add(categoryDomain);
        }


        return result;
    }




}
