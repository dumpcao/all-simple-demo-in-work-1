package com.learn.model;

import lombok.Data;

@Data
public class CategoryDomain {
    /**
     * 类别的中文名称
     */
    private String categoryChineseName;

    /**
     * 超链接
     */
    private String href;

    /**
     * 该类别下的博客数量
     */
    private Integer blogCountOfTheCategory;
}
