package com.learn.model;

import lombok.Data;

@Data
public class BlogDomain {
    /**
     * 博客标题
     */
    private String blogTitle;

    /**
     * 超链接
     */
    private String href;

    /**
     * 博客的所属类别，这里用类别的名称
     */
    private String categoryName;
}
