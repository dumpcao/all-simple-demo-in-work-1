package com.example;

import javax.servlet.http.HttpSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableRedisHttpSession
@RestController
public class App {
//    @Bean
//    public RedisConnectionFactory redisConnectionFactory(){
//
//    }
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping("setMsg")
    public String  setMsg(HttpSession session) {
        session.setAttribute("msg", "Hello SpringSession!");
        return "ok";
    }
    @Bean("springSessionDefaultRedisSerializer")
    public RedisSerializer setSerializer(){
        return new GenericJackson2JsonRedisSerializer();
    }

    @RequestMapping("getMsg")
    public String  getMsg(HttpSession session) {
        String msg=(String) session.getAttribute("msg");
        return msg;
    }
}
