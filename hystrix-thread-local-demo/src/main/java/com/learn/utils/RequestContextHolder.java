package com.learn.utils;

import com.learn.vo.UserVO;

public class RequestContextHolder {
    static ThreadLocal<UserVO> userVOThreadLocal = new ThreadLocal<>();

    public static void set(UserVO userVO){
        userVOThreadLocal.set(userVO);
    }

    public static UserVO get(){
        return userVOThreadLocal.get();
    }


}
