package com.learn.utils;

import com.learn.vo.UserVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SessionUtils {

    public static UserVO getSessionVOFromRedisAndPut2ThreadLocal() {
        UserVO userVO = new UserVO();
        userVO.setUserName("test user");

        RequestContextHolder.set(userVO);
        log.info("set thread local:{} to context",userVO);

        return userVO;
    }
}
