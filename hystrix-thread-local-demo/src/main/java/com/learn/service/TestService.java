package com.learn.service;

import com.learn.utils.RequestContextHolder;
import com.learn.vo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TestService {


    public String getResult() {
        UserVO userVO = RequestContextHolder.get();
        log.info("I am  hystrix pool thread,try to get threadlocal:{}",userVO);

        return userVO.toString();
    }

}
