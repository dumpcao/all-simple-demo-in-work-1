package com.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ServletComponentScan //当存在拦截器的时候，加上这个注解
//@MapperScan("com.lkl.hystrixdemo.order.mapper")
@EnableCaching
public class BootStrapApplication {

    public static void main(String[] args) {
//        HystrixPlugins.getInstance().registerConcurrencyStrategy(new MyHystrixConcurrencyStrategy());
        SpringApplication.run(BootStrapApplication.class, args);
    }

    @Bean
    public RestTemplate getTemplate () {
        return new RestTemplate();
    }
}
