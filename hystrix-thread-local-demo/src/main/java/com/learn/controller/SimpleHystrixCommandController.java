package com.learn.controller;

import com.learn.command.SimpleHystrixCommand;
import com.learn.utils.SessionUtils;
import com.learn.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SimpleHystrixCommandController {

    @Autowired
    TestService testService;



    @RequestMapping("/")
    public String hystrixOrder () {
        SessionUtils.getSessionVOFromRedisAndPut2ThreadLocal();
        SimpleHystrixCommand simpleHystrixCommand = new SimpleHystrixCommand(testService);
        String res = simpleHystrixCommand.execute();
        return res;
    }




}
