package com.learn.controller;

import com.learn.command.observable.HelloWorldSemaphoreIsolationCommand;
import com.learn.command.observable.HelloWorldThreadIsolationCommand;
import com.learn.utils.SessionUtils;
import com.learn.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rx.Observable;

@RestController
@Slf4j
public class HelloWorldController {


    /**
     * 信号量隔离，不会切换线程，不存在thread local问题
     * @return
     */
    @RequestMapping("/hello")
    public String hello () {
        SessionUtils.getSessionVOFromRedisAndPut2ThreadLocal();
        HelloWorldSemaphoreIsolationCommand bean =
                SpringContextUtils.getBean(HelloWorldSemaphoreIsolationCommand.class);
        Observable<String> observable = bean.toObservable();
        String s = observable.toBlocking().single();

        return s;
    }

    /**
     * 线程隔离
     * @return
     */
    @RequestMapping("/helloByThread")
    public String helloByThread () {
        SessionUtils.getSessionVOFromRedisAndPut2ThreadLocal();
        HelloWorldThreadIsolationCommand bean =
                SpringContextUtils.getBean(HelloWorldThreadIsolationCommand.class);
        Observable<String> observable = bean.toObservable();
        String s = observable.toBlocking().single();

        return s;
    }


}