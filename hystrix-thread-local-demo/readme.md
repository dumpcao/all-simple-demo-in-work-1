# 注册插件的方式
1. 直接调用
```java
HystrixPlugins.getInstance().registerConcurrencyStrategy(new MyHystrixConcurrencyStrategy());
```
2. 通过本案例中的jdk的spi方式注册
在resources下，META-INF/services中注册了com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategy的实现类

具体的spi实现，可以查看hystrix的实现代码：
```java
public HystrixConcurrencyStrategy getConcurrencyStrategy() {
        if (concurrencyStrategy.get() == null) {
            // check for an implementation from Archaius first
            // 1
            Object impl = getPluginImplementation(HystrixConcurrencyStrategy.class);
            if (impl == null) {
                // nothing set via Archaius so initialize with default
                concurrencyStrategy.compareAndSet(null, HystrixConcurrencyStrategyDefault.getInstance());
                // we don't return from here but call get() again in case of thread-race so the winner will always get returned
            } else {
                // we received an implementation from Archaius so use it
                concurrencyStrategy.compareAndSet(null, (HystrixConcurrencyStrategy) impl);
            }
        }
        return concurrencyStrategy.get();
    }
```
上图，1处，会去查找实现类：
```java
    private <T> T getPluginImplementation(Class<T> pluginClass) {
        // 1
        T p = getPluginImplementationViaProperties(pluginClass, dynamicProperties);
        if (p != null) return p;        
        // 2
        return findService(pluginClass, classLoader);
    }
```
如上，1处，会去动态属性中获取，这个动态属性，怎么动态呢？具体的了解不多，如果集成了Netflix Archaius组件，Netflix Archaius
可以动态修改属性，然后这里就能感知到。
如果1处查找失败，会去2处查找，2处就是jdk的spi机制，也是我们demo中使用的方式。

关于插件相关的东西，更多可以查看：
https://github.com/Netflix/Hystrix/wiki/Plugins

