package com.learn.service;

import com.learn.entity.Users;

public interface IUsersService{


    void deleteUser(long userId);

    Users getUser(long userId);
}
