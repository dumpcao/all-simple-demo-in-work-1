CREATE DATABASE `test` /*!40100 DEFAULT CHARACTER SET utf8 */


CREATE TABLE `operation_log` (
  `log_id` bigint(20) NOT NULL,
  `method_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operate_id` bigint(20) DEFAULT NULL,
  `operate_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operate_time` datetime DEFAULT NULL,
  `operate_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operate_type_id` tinyint(10) DEFAULT NULL COMMENT '操作类型ID',
  `record_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据主键ID',
  `remark` text COLLATE utf8mb4_unicode_ci,
  `tab_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='operation_log'


CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL COMMENT '用户Id',
  `user_name` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名称',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户表'